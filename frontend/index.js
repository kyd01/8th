window.jQuery = require( 'jquery' );
const $ = window.jQuery;
require( './bootstrap' );
require( './index.sass' );
require( 'bootstrap-sass/assets/javascripts/bootstrap/modal' );

$( document ).ready( () => {
    console.log( 'Начни набирать свою фамилию здесь. Метод должен заканчиваться круглыми скобками ()' );

    const $modal = $( '#modal' );

    function unblockPower( lastName ) {
        $modal.find( '.power' ).hide();
        $modal.find( '#' + lastName + '-power' ).show();
        $modal.modal( 'show' );
    }

    window.anisimovaUnblockPower = () => unblockPower( 'anisimova' );
    window.polyaninaUnblockPower = () => unblockPower( 'polyanina' );
    window.neganovaUnblockPower = () => unblockPower( 'neganova' );
    window.betsUnblockPower = () => unblockPower( 'bets' );
} );