'use strict';

const entries = {
    'main': './frontend'
};

const externals = {
};

const includes = [
    'frontend'
];

const urlImageMaxBytes = 1000000000;

const urlFontMaxBytes = 1000000000;

process.noDeprecation = true;

module.exports = function( env ) {
    env = env || {};

    const webpack = require( 'webpack' );

    const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );
    const extractTextPlugin = new ExtractTextPlugin( './assets/css/[name].css' );

    const path = require( 'path' );
    let _includes = [];
    for( let index in includes ) {
        if( includes.hasOwnProperty( index ) ) {
            _includes.push( path.resolve( __dirname, includes[index] ) );
        }
    }

    let config = {
        entry: entries,
        output: {
            filename: './assets/js/[name].js'
        },
        externals: externals,
        module: {
            rules: [
                {
                    test: /\.js$/,
                    include: _includes,
                    loader: 'babel-loader',
                    query: {
                        presets: ['es2015']
                    }
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/,
                    include: _includes,
                    use: {
                        loader: 'url-loader',
                        options: {
                            limit: urlImageMaxBytes,
                            name: '[name].[ext]',
                            outputPath: 'assets/images/',
                            publicPath: '/8th/assets/images/'
                        }
                    }
                },
                {
                    test: /\.(eot|ttf|otf|woff|woff2)$/,
                    include: _includes,
                    use: {
                        loader: 'url-loader',
                        options: {
                            limit: urlFontMaxBytes,
                            name: '[name].[ext]',
                            outputPath: 'assets/fonts/',
                            publicPath: '/8th/assets/fonts/'
                        }
                    }
                },
                {
                    test: /(\.css$|\.s[ac]ss$)/,
                    include: _includes,
                    use: extractTextPlugin.extract( {
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: { minimize: !! env.production }
                            },
                            'sass-loader'
                        ]
                    } )
                }
            ]
        },
        plugins: [
            extractTextPlugin,
            new webpack.DefinePlugin( {
                DEBUG: ! env.production
            } )
        ]
    };

    if( env.production ) {
        const uglifyJsPlugin = new webpack.optimize.UglifyJsPlugin( {
            compress: {
                warnings: false,
                unsafe: true,
                drop_console: false
            }
        } );

        config.plugins.push( uglifyJsPlugin );
    }
    else {
        config.watch = true;
        config.devtool = 'source-map';
    }

    return config;
};